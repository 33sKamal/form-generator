const mix = require('laravel-mix');


mix.disableSuccessNotifications()
.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css')
    
    // form generator assets
    .js('resources/js/form-generator.js', 'public/js')

    .version()
