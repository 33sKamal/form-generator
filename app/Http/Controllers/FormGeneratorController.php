<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FormGeneratorController extends Controller
{

    /**
     * return the form generator index page
     */
    public function index()
    {
        return view('form-generator');
    }
}
